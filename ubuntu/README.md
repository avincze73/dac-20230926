# Setup Ubuntu 22.04
- Ubuntu 22.04 server can installed as virtual machine using virtual box, vmware or other virtualization solution.
- Download ubuntu server image from here: \
https://releases.ubuntu.com/22.04.3/ubuntu-22.04.3-live-server-amd64.iso?_ga=2.22971134.438869203.1693916096-453674183.1693916096
- Install the server with bridge network. The vm should access the internet and the host should connect to the vm.


## Post installation tasks
```bash
#execute commands as root user
adduser sprintadm
usermod -aG sudo sprintadm

#execute commands as sprintadm user
sudo apt update && sudo apt upgrade -y &&
    sudo timedatectl set-timezone Europe/Budapest
sudo apt install -y default-jdk maven zsh 
zsh --version
sudo usermod -s $(which zsh) sprintadm
sh -c "$(curl -fsSL https://raw.githubusercontent.com/robbyrussell/oh-my-zsh/master/tools/install.sh)"

mkdir git && cd git && git config --global credential.helper store
```
## Docker Hub account
- Create a docker hub account on https://hub.docker.com



[Főoldal](../README.md)
